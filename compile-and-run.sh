#!/bin/sh
#/**
# * Shell Script para compilar o Very Small sem precisar do Eclipse
# * @type {[type]}
# */
make clean 
rm -rf  ./Debug/Auxiliares.o ./Debug/Captura.o ./Debug/Controle.o ./Debug/Estrategia.o ./Debug/Futebol.o ./Debug/Posicoes.o ./Debug/Serial.o  ./Debug/Auxiliares.d ./Debug/Captura.d ./Debug/Controle.d ./Debug/Estrategia.d ./Debug/Futebol.d ./Debug/Posicoes.d ./Debug/Serial.d  Debug/VerySmall_2015

make all
#Linking and Object File Generating
#Auxiliares
g++ -O0 -g3 -Wall -c -fmessage-length=0 `pkg-config gtkmm-2.4 --cflags` -MMD -MP -MF"Debug/Auxiliares.d" -MT"Debug/Auxiliares.d" -o "Debug/Auxiliares.o" "Auxiliares.cpp"
#Captura
g++ -O0 -g3 -Wall -c -fmessage-length=0 `pkg-config gtkmm-2.4 --cflags` -MMD -MP -MF"Debug/Captura.d" -MT"Debug/Captura.d" -o "Debug/Captura.o" "Captura.cpp"
#Controle
g++ -O0 -g3 -Wall -c -fmessage-length=0 `pkg-config gtkmm-2.4 --cflags` -MMD -MP -MF"Debug/Controle.d" -MT"Debug/Controle.d" -o "Debug/Controle.o" "Controle.cpp";
#Estrategia
g++ -O0 -g3 -Wall -c -fmessage-length=0 `pkg-config gtkmm-2.4 --cflags` -MMD -MP -MF"Debug/Estrategia.d" -MT"Debug/Estrategia.d" -o "Debug/Estrategia.o" "Estrategia.cpp";
#Futebol
g++ -O0 -g3 -Wall -c -fmessage-length=0 `pkg-config gtkmm-2.4 --cflags` -MMD -MP -MF"Debug/Futebol.d" -MT"Debug/Futebol.d" -o "Debug/Futebol.o" "Futebol.cpp";
#Posicoes
g++ -O0 -g3 -Wall -c -fmessage-length=0 `pkg-config gtkmm-2.4 --cflags` -MMD -MP -MF"Debug/Posicoes.d" -MT"Debug/Posicoes.d" -o "Debug/Posicoes.o" "Posicoes.cpp";
#Serial
g++ -O0 -g3 -Wall -c -fmessage-length=0 `pkg-config gtkmm-2.4 --cflags` -MMD -MP -MF"Debug/Serial.d" -MT"Debug/Serial.d" -o "Debug/Serial.o" "Serial.cpp";
#Compiling VerySmall
g++ -o "Debug/VerySmall_2015"  ./Debug/Auxiliares.o ./Debug/Captura.o ./Debug/Controle.o ./Debug/Estrategia.o ./Debug/Futebol.o ./Debug/Posicoes.o ./Debug/Serial.o    `pkg-config gtkmm-2.4 --`;

cd Debug;

sh VerySmall_2015;